const mongoose = require("../db");
const userSchema = mongoose.Schema(
  {
    email: {
      type: String,
      required: [true, "email is required!"],
    },
    password: {
      type: String,
      required: [true, "Password is required!"],
    },
    name: {
      type: String,
      minLength: [3, "Please provide a name of at least 3 characters!"],
      maxLength: [20, "Name cannot be more than 20 characters"],
    },
  },
  { timestamps: true }
);
const userModel = mongoose.model("users", userSchema);
module.exports = userModel;
