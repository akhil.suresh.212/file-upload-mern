const router = require("express").Router();
var path = require("path");
const fileModel = require("../model/file-model");
require("dotenv").config();

router.post("/", (req, res) => {
  console.log({ files: req.files });
  if (req.files) {
    myFile = req.files.myFile;
    filename = "IMAGE-" + Date.now() + path.extname(myFile.name);
    console.log({ filename });
    myFile.mv(process.env.UPLOAD_PATH + "/" + filename, (err) => {
      if (err) {
        res.json({ err: "something went wrong" });
      } else {
        newFile = new fileModel({ fileName: filename });
        newFile.save();
        fileModel.find({}, (err, docs) => {
          if (!err) res.send(docs);
          else res.json({ err });
        });
      }
    });
  }
});

router.get("/", (req, res) => {
  fileModel.find({}, (err, docs) => {
    if (!err) res.send(docs);
    else res.json({ err });
  });
});

module.exports = router;
