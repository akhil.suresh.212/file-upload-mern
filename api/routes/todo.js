const router = require("express").Router();
const todoModel = require("../model/todo-model");
router.get("/", (req, res) => {
  const {
    params: { userId },
  } = req;

  todoModel.find({}, (err, doc) => {
    res.send(doc);
  });
});

router.post("/add", (req, res) => {
  const {
    body: { userId, title, desc },
  } = req;

  new todoModel(req.body).save((err, doc) => {
    res.send({ err, doc });
  });
});

module.exports = router;
