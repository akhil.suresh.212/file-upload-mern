import React, { Component } from "react";
import axios from "axios";
export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h3>Sign Up</h3>

        <div className="form-group">
          <label>First name</label>
          <input
            name="name"
            type="text"
            onChange={this.handleChange.bind(this)}
            className="form-control"
            placeholder="First name"
          />
        </div>
        <div className="form-group">
          <label>Email address</label>
          <input
            name="email"
            onChange={this.handleChange.bind(this)}
            type="email"
            className="form-control"
            placeholder="Enter email"
          />
        </div>

        <div className="form-group">
          <label>Password</label>
          <input
            name="password"
            type="password"
            onChange={this.handleChange.bind(this)}
            className="form-control"
            placeholder="Enter password"
          />
        </div>

        <button type="submit" className="btn btn-primary btn-block">
          Sign Up
        </button>
        <p className="forgot-password text-right">
          Already registered <a href="#">sign in?</a>
        </p>
      </form>
    );
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.submitForm();
  };

  submitForm = async () => {
    console.log({ state: this.state });
    const response = await axios.post(
      "http://localhost:3001/user/register",
      this.state,
      {
        headers: {
          "content-Type": "Application/json",
        },
      }
    );

    console.log(response);
  };
}
